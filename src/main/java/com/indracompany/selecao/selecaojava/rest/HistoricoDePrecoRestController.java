package com.indracompany.selecao.selecaojava.rest;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.indracompany.selecao.selecaojava.entity.Bandeira;
import com.indracompany.selecao.selecaojava.entity.Combustivel;
import com.indracompany.selecao.selecaojava.entity.Empresa;
import com.indracompany.selecao.selecaojava.entity.Estado;
import com.indracompany.selecao.selecaojava.entity.HistoricoDePreco;
import com.indracompany.selecao.selecaojava.entity.InformacoesCombustivel;
import com.indracompany.selecao.selecaojava.entity.Municipio;
import com.indracompany.selecao.selecaojava.entity.Regiao;
import com.indracompany.selecao.selecaojava.service.BandeiraService;
import com.indracompany.selecao.selecaojava.service.CombustivelService;
import com.indracompany.selecao.selecaojava.service.EmpresaService;
import com.indracompany.selecao.selecaojava.service.EstadoService;
import com.indracompany.selecao.selecaojava.service.HistoricoDePrecoService;
import com.indracompany.selecao.selecaojava.service.InformacoesCombustivelService;
import com.indracompany.selecao.selecaojava.service.MunicipioService;
import com.indracompany.selecao.selecaojava.service.RegiaoService;

@RestController
@RequestMapping("/api/historico-de-precos")
public class HistoricoDePrecoRestController {

	private BandeiraService bandeiraService;
	private CombustivelService combustivelService;
	private EmpresaService empresaService;
	private EstadoService estadoService;
	private HistoricoDePrecoService historicoService;
	private InformacoesCombustivelService infoService;
	private MunicipioService municipioService;
	private RegiaoService regiaoService;
	
	@Autowired
	public HistoricoDePrecoRestController(BandeiraService bandeiraService, CombustivelService combustivelService,
			EmpresaService empresaService, EstadoService estadoService, HistoricoDePrecoService historicoService,
			InformacoesCombustivelService infoService, MunicipioService municipioService,
			RegiaoService regiaoService) {
		this.bandeiraService = bandeiraService;
		this.combustivelService = combustivelService;
		this.empresaService = empresaService;
		this.estadoService = estadoService;
		this.historicoService = historicoService;
		this.infoService = infoService;
		this.municipioService = municipioService;
		this.regiaoService = regiaoService;
	}
	
	
	// GET "/api/historico-de-precos" retorna uma lista dos historicos
	@GetMapping("")
	public ArrayList<Map<String, String>> listHistoricos() {
		
		ArrayList<Map<String, String>> resultado = new ArrayList<>();
		
		List<HistoricoDePreco> historicos = historicoService.listAll();
		
		for (HistoricoDePreco historico : historicos) {
			Map<String, String> map = new HashMap<>();
			
			InformacoesCombustivel info = historico.getInfoCombustivel();
			
			Combustivel combustivel = info.getCombustivel();
			Bandeira bandeira = info.getBandeira();
			Empresa empresa = info.getEmpresa();
			
			Municipio municipio = empresa.getMunicipio();
			Estado estado = municipio.getEstado();
			Regiao regiao = estado.getRegiao();
			
			map.put("regiao", regiao.getSigla());
			map.put("estado", estado.getSigla());
			map.put("municipio", municipio.getNome());
			map.put("empresa", empresa.getNome());
			map.put("codigo", Integer.toString(info.getCodigo()));
			map.put("produto", combustivel.getNome());
			map.put("data da coleta", historico.getDataColeta().toString());
			map.put("valor da compra", historico.getValorCompra().toString());
			map.put("valor da venda", historico.getValorVenda().toString());
			map.put("unidade de medida", combustivel.getUnidadeDeMedida());
			map.put("bandeira", bandeira.getNome());
			
			resultado.add(map);			
		}

		return resultado;
	}
	
	// GET "/api/historico-de-precos/{historicoId}" retorna o historico correspondente
	@GetMapping("/{historicoId}")
	public HistoricoDePreco getHistorico(@PathVariable int historicoId) {
		return historicoService.getById(historicoId);
	}
	
	
	// GET "/api/historico-de-precos/{dia}/{mes}/{ano}" retorna o historico correspondente a data
	@GetMapping("/{dia}/{mes}/{ano}")
	public List<HistoricoDePreco> getHistorico(@PathVariable String dia, @PathVariable String mes,
										@PathVariable String ano) {

		Date dataColeta = Date.valueOf(ano + "-" + mes + "-" + dia);
		
		List<HistoricoDePreco> resultado = historicoService.getAllByDate(dataColeta);
		
		return resultado;
	}
	
	// POST "/api/historico-de-precos" adiciona um historico
	@PostMapping("")
	public HistoricoDePreco addHistorico(@RequestBody HistoricoDePreco historico) {

		// obriga o id do historico ser 0 para forcar a adicao do registro
		historico.setId(0);
		
		historicoService.addOrUpdate(historico);
		
		return historico;
	}
	
	// PUT "/api/historico-de-precos" atualiza um historico no banco
	@PutMapping("")
	public HistoricoDePreco updateHistorico(@RequestBody HistoricoDePreco historico) {

		// checa se o usuario realmente existe
		if (getHistorico(historico.getId()) == null) {
			throw new RuntimeException("ERROR! Historico " + historico.getId() + " not found!");
		}

		historicoService.addOrUpdate(historico);

		return historico;		
	}
	

	// DELETE "/api/historico-de-precos/{historicoId}" exclui um historico do banco
	@DeleteMapping("/{historicoId}")
	public String deleteHistorico(@PathVariable int historicoId) {

		HistoricoDePreco historico = historicoService.getById(historicoId);
		
		if (historico == null) {
			throw new RuntimeException("ERROR! Historico " + historicoId + " not found!");
		}
		
		historicoService.deleteById(historicoId);
		
		return "SUCCESS! Historico " + historicoId + " deleted!";
	}
	
	// GET "/api/historico-de-precos/historico-empresas/{empresaNome}" retorna o historico da empresa
	@GetMapping("/historico-empresas/{empresaNome}")
	public List<HistoricoDePreco> getHistoricoByEmpresa(@PathVariable String empresaNome) {
		
		List<HistoricoDePreco> resultado = new ArrayList<>();
		
		Empresa empresa = empresaService.getByName(empresaNome);
		
		empresa.setInfoCombustiveis(infoService.getByAllByIdEmpresa(empresa.getId()));
		
		for (InformacoesCombustivel info : empresa.getInfoCombustiveis()) {
			
			resultado.addAll(historicoService.getAllByInfoCodigo(info.getCodigo()));
			
		}

		return resultado;
	}
	
	// GET "/api/historico-de-precos/historico-regioes/{siglaRegiao}" retorna o historico da regiao
	@GetMapping("/historico-regioes/{siglaRegiao}")
	public List<HistoricoDePreco> getHistoricoRegiao(@PathVariable String siglaRegiao) {
		
		List<HistoricoDePreco> resultado = new ArrayList<>();
		
		Regiao regiao = regiaoService.getByInitials(siglaRegiao);
		
		regiao.setEstados(estadoService.getAllByRegiaoId(regiao.getId()));
		
		for(Estado estado : regiao.getEstados()) {
			estado.setMunicipios(municipioService.getAllByEstadoId(estado.getId()));
			
			for (Municipio municipio : estado.getMunicipios()) {
				municipio.setEmpresas(empresaService.getAllByMunicipioId(municipio.getId()));
				
				for (Empresa empresa : municipio.getEmpresas()) {
					empresa.setInfoCombustiveis(infoService.getByAllByIdEmpresa(empresa.getId()));
					
					for (InformacoesCombustivel info : empresa.getInfoCombustiveis()) {
						info.setHistorico(historicoService.getAllByInfoCodigo(info.getCodigo()));
						resultado.addAll(info.getHistorico());
					}
				}
			}
		}
		
		return resultado;
	}
	
	// GET "/api/historico-de-precos/media-precos-bandeira/{nomeBandeira}" retorna a media de precos por bandeira
	@GetMapping("/media-precos-bandeira/{nomeBandeira}")
	public Map<String, Float> getMediaPrecosBandeira(@PathVariable String nomeBandeira) {
		Map <String, Float> medias = new HashMap<>();
		
		Bandeira bandeira = bandeiraService.getByName(nomeBandeira);
		
		List<InformacoesCombustivel> infos = infoService.getByAllByIdBandeira(bandeira.getId());
		
		List<HistoricoDePreco> historicos = new ArrayList<>();
		
		for (InformacoesCombustivel info : infos) {
			historicos.addAll(historicoService.getAllByInfoCodigo(info.getCodigo()));
		}
		
		Float mediaCompra = Float.valueOf(0);
		Float mediaVenda = Float.valueOf(0);
		
		int numComprasValidas = 0;
		int numVendasValidas = 0;
		
		// calcular média
		for (HistoricoDePreco historico : historicos) {
			
			// verifica se o valor existe no banco
			if (historico.getValorCompra() != 0) {
				mediaCompra += historico.getValorCompra();
				numComprasValidas++;
			}

			if (historico.getValorVenda() != 0) {
				mediaVenda += historico.getValorVenda();
				numVendasValidas++;
			}

		}
		
		mediaCompra = (mediaCompra/numComprasValidas);
		mediaVenda = (mediaVenda/numVendasValidas);
		
		medias.put("mediaCompra", mediaCompra);
		medias.put("mediaVenda", mediaVenda);

		
		return medias;		
	}
	
	// GET "/api/historico-de-precos/media-precos-municipio/{siglaEstado}/{nomeMunicipio}" retorna a media de precos por municipio
	@GetMapping("/media-precos-municipio/{siglaEstado}/{nomeMunicipio}")
	public Map<String, Float> getMediaPrecosMunicipio(@PathVariable String siglaEstado, @PathVariable String nomeMunicipio) {
		Map <String, Float> medias = new HashMap<>();
		
		Estado estado = estadoService.getByInitials(siglaEstado);
		
		Municipio municipio = municipioService.getByNameAndEstadoId(nomeMunicipio, estado.getId());
		
		List<Empresa> empresas = empresaService.getAllByMunicipioId(municipio.getId());
		
		List<InformacoesCombustivel> infos = new ArrayList<>();
		
		for (Empresa empresa : empresas) {
			infos.addAll(infoService.getByAllByIdEmpresa(empresa.getId()));
		}
		
		List<HistoricoDePreco> historicos = new ArrayList<>();
		
		for (InformacoesCombustivel info : infos) {
			historicos.addAll(historicoService.getAllByInfoCodigo(info.getCodigo()));
		}
		
		Float mediaCompra = Float.valueOf(0);
		Float mediaVenda = Float.valueOf(0);
		
		int numComprasValidas = 0;
		int numVendasValidas = 0;
		
		// calcular média
		for (HistoricoDePreco historico : historicos) {
			
			// verifica se o valor existe no banco
			if (historico.getValorCompra() != 0) {
				mediaCompra += historico.getValorCompra();
				numComprasValidas++;
			}

			if (historico.getValorVenda() != 0) {
				mediaVenda += historico.getValorVenda();
				numVendasValidas++;
			}

		}
		
		mediaCompra = (mediaCompra/numComprasValidas);
		mediaVenda = (mediaVenda/numVendasValidas);
		
		medias.put("mediaCompra", mediaCompra);
		medias.put("mediaVenda", mediaVenda);

		
		return medias;		
	}
	
	
}

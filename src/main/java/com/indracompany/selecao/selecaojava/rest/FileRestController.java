package com.indracompany.selecao.selecaojava.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.indracompany.selecao.selecaojava.entity.Bandeira;
import com.indracompany.selecao.selecaojava.entity.Combustivel;
import com.indracompany.selecao.selecaojava.entity.Empresa;
import com.indracompany.selecao.selecaojava.entity.Estado;
import com.indracompany.selecao.selecaojava.entity.HistoricoDePreco;
import com.indracompany.selecao.selecaojava.entity.InformacoesCombustivel;
import com.indracompany.selecao.selecaojava.entity.Municipio;
import com.indracompany.selecao.selecaojava.entity.Regiao;
import com.indracompany.selecao.selecaojava.service.BandeiraService;
import com.indracompany.selecao.selecaojava.service.CombustivelService;
import com.indracompany.selecao.selecaojava.service.EmpresaService;
import com.indracompany.selecao.selecaojava.service.EstadoService;
import com.indracompany.selecao.selecaojava.service.HistoricoDePrecoService;
import com.indracompany.selecao.selecaojava.service.InformacoesCombustivelService;
import com.indracompany.selecao.selecaojava.service.MunicipioService;
import com.indracompany.selecao.selecaojava.service.RegiaoService;

@RestController
@RequestMapping("/api/arquivo")
public class FileRestController {

	private BandeiraService bandeiraService;
	private CombustivelService combustivelService;
	private EmpresaService empresaService;
	private EstadoService estadoService;
	private HistoricoDePrecoService historicoService;
	private InformacoesCombustivelService infoService;
	private MunicipioService municipioService;
	private RegiaoService regiaoService;
	
	@Autowired
	public FileRestController(BandeiraService bandeiraService, CombustivelService combustivelService,
			EmpresaService empresaService, EstadoService estadoService, HistoricoDePrecoService historicoService,
			InformacoesCombustivelService infoService, MunicipioService municipioService,
			RegiaoService regiaoService) {
		this.bandeiraService = bandeiraService;
		this.combustivelService = combustivelService;
		this.empresaService = empresaService;
		this.estadoService = estadoService;
		this.historicoService = historicoService;
		this.infoService = infoService;
		this.municipioService = municipioService;
		this.regiaoService = regiaoService;
	}
	
	
	// POST "/api/arquivo/importar-csv" importa o arquivo csv para o banco de dados
	@PostMapping("/importar-csv")
	public String importCSVFile(@RequestParam("csvFile") MultipartFile csvFile) {
		
		BufferedReader reader;

		try {

			InputStream is = csvFile.getInputStream();
		    reader = new BufferedReader(new InputStreamReader(is));
			
			// ler linha por linha do arquivo começando da segunda
			String line = reader.readLine();
			line = reader.readLine();
			
			while (line != null) {
				
				// separar cada elemento da linha por "dois espaços"
				String[] palavras = line.split("  ");
				

				Regiao regiao = new Regiao(palavras[0]);
				
				if (regiaoService.getByInitials(regiao.getSigla()) != null) {
					regiao = regiaoService.getByInitials(regiao.getSigla());
				}
				
				Estado estado = new Estado(palavras[1], regiao);
				
				if (estadoService.getByInitials(estado.getSigla()) != null) {
					estado = estadoService.getByInitials(estado.getSigla());					
				}
				
				Municipio municipio = new Municipio(palavras[2], estado);

				if (municipioService.getByName(municipio.getNome()) != null) {
					if (municipioService.getByName(municipio.getNome()).getEstado().getSigla() == estado.getSigla()) {
						municipio = municipioService.getByName(municipio.getNome());
					}
				}
				
				Empresa empresa = new Empresa(palavras[3], municipio);

				if (empresaService.getByName(empresa.getNome()) != null) {
					empresa = empresaService.getByName(empresa.getNome());					
				}
				
				Combustivel combustivel = new Combustivel(palavras[5], palavras[9]);

				if (combustivelService.getByName(combustivel.getNome()) != null) {
					combustivel = combustivelService.getByName(combustivel.getNome());					
				}
				
				Bandeira bandeira = new Bandeira(palavras[10]);

				if (bandeiraService.getByName(bandeira.getNome()) != null) {
					bandeira = bandeiraService.getByName(bandeira.getNome());					
				}
				
				InformacoesCombustivel infoCombustivel = new InformacoesCombustivel(Integer.valueOf(palavras[4]), empresa, combustivel, bandeira);
				
				// tratamento de data				
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		       
				Date dataColeta = null;
				
				try {
					dataColeta = formatter.parse(palavras[6]);
		        } catch (ParseException e) {
		            e.printStackTrace();
		        }


				// tratamento do valor da compra vazio
				Float valorCompra = Float.valueOf(0);

				if (!palavras[7].isEmpty()) {
					valorCompra = Float.valueOf(palavras[7].replace(",","."));
				}

				Float valorVenda = Float.valueOf(palavras[8].replace(",","."));

				HistoricoDePreco historico = new HistoricoDePreco(infoCombustivel, dataColeta, valorCompra, valorVenda);
				
				historicoService.addOrUpdate(historico);
				
				line = reader.readLine();
			}
			
			reader.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return "SUCCESS! Your file is in the Database!";
	}		
}

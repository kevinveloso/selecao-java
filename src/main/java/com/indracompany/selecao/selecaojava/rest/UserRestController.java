package com.indracompany.selecao.selecaojava.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.indracompany.selecao.selecaojava.entity.User;
import com.indracompany.selecao.selecaojava.service.UserService;

@RestController
@RequestMapping("/api/usuarios")
public class UserRestController {
	
	private UserService userService;
	
	@Autowired
	public UserRestController(UserService userService) {
		this.userService = userService;
	}
	
	// GET "/api/usuarios" retorna uma lista dos usuarios
	@GetMapping("")
	public List<User> listAllUsers() {
		return userService.listAll();
	}
	
	// GET "/api/usuarios/{userId}" retorna o usuario correspondente
	@GetMapping("/{userId}")
	public User getUser(@PathVariable int userId) {
		User user = userService.getById(userId);
		
		// verifica se o usuario existe no banco
		if (user == null) {
			throw new RuntimeException("ERROR! User " + userId + " not found!");
		}
		
		return user;
	}
	
	// POST "/api/usuarios" adiciona um usuario ao banco
	@PostMapping("")
	public User addUser(@RequestBody User user) {

		// obriga o id do usuario a ser 0 para forcar a adicao do registro
		user.setId(0);
		
		// encriptando a senha
		String securePassword = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(12));
		user.setPassword(securePassword);
		
		userService.addOrUpdate(user);
		
		return user;
	}
	
	// PUT "/api/usuarios" atualiza um usuario no banco
	@PutMapping("")
	public User updateUser(@RequestBody User user) {
		
		// checa se o usuario realmente existe
		if (getUser(user.getId()) == null) {
			throw new RuntimeException("ERROR! User " + user.getId() + " not found!");
		}
		
		userService.addOrUpdate(user);
		
		return user;		
	}
	
	// DELETE "/api/usuarios/{userId}" exclui um usuario do banco
	@DeleteMapping("/{userId}")
	public String deleteUser(@PathVariable int userId) {

		User user = userService.getById(userId);
		
		if (user == null) {
			throw new RuntimeException("ERROR! User " + userId + " not found!");
		}
		
		userService.deleteById(userId);
		
		return "SUCCESS! User " + userId + " deleted!";
	}

}

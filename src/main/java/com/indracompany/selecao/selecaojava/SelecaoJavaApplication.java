package com.indracompany.selecao.selecaojava;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.indracompany.selecao.selecaojava.entity.Bandeira;
import com.indracompany.selecao.selecaojava.entity.Combustivel;
import com.indracompany.selecao.selecaojava.entity.Empresa;
import com.indracompany.selecao.selecaojava.entity.Estado;
import com.indracompany.selecao.selecaojava.entity.HistoricoDePreco;
import com.indracompany.selecao.selecaojava.entity.InformacoesCombustivel;
import com.indracompany.selecao.selecaojava.entity.Municipio;
import com.indracompany.selecao.selecaojava.entity.Regiao;

@SpringBootApplication
public class SelecaoJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SelecaoJavaApplication.class, args);
	}

}

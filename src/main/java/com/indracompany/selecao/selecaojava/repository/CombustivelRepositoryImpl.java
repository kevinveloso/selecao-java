package com.indracompany.selecao.selecaojava.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.indracompany.selecao.selecaojava.entity.Combustivel;

@Repository
public class CombustivelRepositoryImpl implements CombustivelRepository {

	// dependencia entitymanager
	private EntityManager entityManager;
	
	// injecao no construtor
	@Autowired
	public CombustivelRepositoryImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	public List<Combustivel> listAll() {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);

		// criar uma query de listagem
		Query<Combustivel> query = currentSession.createQuery("from Combustivel", Combustivel.class);

		// executar query e guardar o resultado
		List<Combustivel> results = query.getResultList();

		return results;
	}

	@Override
	public Combustivel getById(int id) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// selecionar bandeira do banco
		Combustivel combustivel = currentSession.get(Combustivel.class, id);
		
		return combustivel;
	}

	@Override
	public void addOrUpdate(Combustivel combustivel) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// limpando a sessao
		currentSession.flush();
		currentSession.clear();
		
		// adicionar ou atualizar combustivel
		currentSession.save(combustivel);
	}

	@Override
	public void deleteById(int id) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// apagar combustivel por id
		Query theQuery = currentSession.createQuery("delete from Combustivel where id=:combustivelId");
		theQuery.setParameter("combustivelId", id);
		
		theQuery.executeUpdate();
	}

	@Override
	public Combustivel getByName(String nome) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);

		// pega combustivel por nome
		Query theQuery = currentSession.createQuery("from Combustivel where lower(nome)=:nome");
		theQuery.setParameter("nome", nome.toLowerCase());

		try {
			Combustivel combustivel = (Combustivel) theQuery.getSingleResult();
			return combustivel;
		} catch (NoResultException e) {
			return null;
		}

	}

}

package com.indracompany.selecao.selecaojava.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.indracompany.selecao.selecaojava.entity.Bandeira;

@Repository
public class BandeiraRepositoryImpl implements BandeiraRepository {

	// dependencia entitymanager
	private EntityManager entityManager;
	
	// injecao no construtor
	@Autowired
	public BandeiraRepositoryImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public List<Bandeira> listAll() {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);

		// criar uma query de listagem
		Query<Bandeira> query = currentSession.createQuery("from Bandeira", Bandeira.class);

		// executar query e guardar o resultado
		List<Bandeira> results = query.getResultList();

		return results;
	}

	@Override
	public Bandeira getById(int id) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// selecionar bandeira do banco
		Bandeira bandeira = currentSession.get(Bandeira.class, id);
		
		return bandeira;
	}

	@Override
	public void addOrUpdate(Bandeira bandeira) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// limpando a sessao
		currentSession.flush();
		currentSession.clear();
		
		// adicionar ou atualizar bandeira
		currentSession.saveOrUpdate(bandeira);
	}

	@Override
	public void deleteById(int id) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// apagar bandeira por id
		Query theQuery = currentSession.createQuery("delete from Bandeira where id=:bandeiraId");
		theQuery.setParameter("bandeiraId", id);
		
		theQuery.executeUpdate();
	}

	@Override
	public Bandeira getByName(String nome) {
		
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// pega bandeira por nome
		Query theQuery = currentSession.createQuery("from Bandeira where lower(nome)=:nome");
		theQuery.setParameter("nome", nome.toLowerCase());
		
		try {
			Bandeira bandeira = (Bandeira) theQuery.getSingleResult();
			return bandeira;
		} catch (NoResultException e) {
			return null;
		}
		
	}

}

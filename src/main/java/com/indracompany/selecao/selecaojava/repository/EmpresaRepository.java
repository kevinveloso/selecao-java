package com.indracompany.selecao.selecaojava.repository;

import java.util.List;

import com.indracompany.selecao.selecaojava.entity.Empresa;

public interface EmpresaRepository {
	public List<Empresa> listAll();
	
	public Empresa getById(int id);
	
	public void addOrUpdate(Empresa empresa);
	
	public void deleteById(int id);
	
	public Empresa getByName(String nome);

	public List<Empresa> getAllByMunicipioId(int id);
}

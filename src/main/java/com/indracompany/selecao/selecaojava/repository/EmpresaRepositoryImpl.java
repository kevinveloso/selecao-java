package com.indracompany.selecao.selecaojava.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.indracompany.selecao.selecaojava.entity.Empresa;

@Repository
public class EmpresaRepositoryImpl implements EmpresaRepository {

	// dependencia entitymanager
	private EntityManager entityManager;
	
	// injecao no construtor
	@Autowired
	public EmpresaRepositoryImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	public List<Empresa> listAll() {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);

		// criar uma query de listagem
		Query<Empresa> query = currentSession.createQuery("from Empresa", Empresa.class);

		// executar query e guardar o resultado
		List<Empresa> results = query.getResultList();

		return results;
	}

	@Override
	public Empresa getById(int id) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// selecionar empresa do banco
		Empresa empresa = currentSession.get(Empresa.class, id);
		
		return empresa;
	}

	@Override
	public void addOrUpdate(Empresa empresa) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// limpando a sessao
		currentSession.flush();
		currentSession.clear();
		
		// adicionar ou atualizar empresa
		currentSession.saveOrUpdate(empresa);
	}

	@Override
	public void deleteById(int id) {

		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// apagar empresa por id
		Query theQuery = currentSession.createQuery("delete from Empresa where id=:empresaId");
		theQuery.setParameter("empresaId", id);
		
		theQuery.executeUpdate();

	}

	@Override
	public Empresa getByName(String nome) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// pega empresa por nome
		Query theQuery = currentSession.createQuery("from Empresa where lower(nome)=:nome");
		theQuery.setParameter("nome", nome.toLowerCase());
		
		try {
			Empresa empresa = (Empresa) theQuery.getSingleResult();
			return empresa;
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public List<Empresa> getAllByMunicipioId(int id) {

		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// pega empresas por municipio
		Query theQuery = currentSession.createQuery("from Empresa where id_municipio=:municipioId");
		theQuery.setParameter("municipioId", id);

		List<Empresa> empresas = theQuery.getResultList();
		return empresas;
	}

}

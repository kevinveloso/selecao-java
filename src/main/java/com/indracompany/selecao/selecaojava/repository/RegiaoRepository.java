package com.indracompany.selecao.selecaojava.repository;

import java.util.List;

import com.indracompany.selecao.selecaojava.entity.Regiao;

public interface RegiaoRepository {
	
	public List<Regiao> listAll();
	
	public Regiao getById(int id);
	
	public void addOrUpdate(Regiao regiao);
	
	public void deleteById(int id);
	
	public Regiao getByInitials(String sigla);
	
}

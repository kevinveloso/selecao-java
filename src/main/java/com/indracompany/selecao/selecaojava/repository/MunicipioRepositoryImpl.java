package com.indracompany.selecao.selecaojava.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.indracompany.selecao.selecaojava.entity.Municipio;

@Repository
public class MunicipioRepositoryImpl implements MunicipioRepository {

	// dependencia entitymanager
	private EntityManager entityManager;
	
	// injecao no construtor
	@Autowired
	public MunicipioRepositoryImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	public List<Municipio> listAll() {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);

		// criar uma query de listagem
		Query<Municipio> query = currentSession.createQuery("from Municipio", Municipio.class);

		// executar query e guardar o resultado
		List<Municipio> results = query.getResultList();

		return results;
	}

	@Override
	public Municipio getById(int id) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// selecionar municipio do banco
		Municipio municipio = currentSession.get(Municipio.class, id);
		
		return municipio;
	}

	@Override
	public void addOrUpdate(Municipio municipio) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// limpando a sessao
		currentSession.flush();
		currentSession.clear();
		
		// adicionar ou atualizar municipio
		currentSession.saveOrUpdate(municipio);
	}

	@Override
	public void deleteById(int id) {

		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// apagar municipio por id
		Query theQuery = currentSession.createQuery("delete from Municipio where id=:municipioId");
		theQuery.setParameter("municipioId", id);
		
		theQuery.executeUpdate();

	}

	@Override
	public Municipio getByName(String nome) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// pega municipio por nome
		Query theQuery = currentSession.createQuery("from Municipio where lower(nome)=:nome");
		theQuery.setParameter("nome", nome.toLowerCase());
		
		try {
			Municipio municipio = (Municipio) theQuery.getSingleResult();
			return municipio;
		} catch (NoResultException e) {
			return null;
		}
	}
	
	@Override
	public Municipio getByNameAndEstadoId(String nome, int estadoId) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// pega municipio por nome
		Query theQuery = currentSession.createQuery("from Municipio where lower(nome)=:nome and id_estado=:estadoId");
		theQuery.setParameter("nome", nome.toLowerCase());
		theQuery.setParameter("estadoId", estadoId);
		
		try {
			Municipio municipio = (Municipio) theQuery.getSingleResult();
			return municipio;
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public List<Municipio> getAllByEstadoId(int id) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// pega municipios por estado
		Query theQuery = currentSession.createQuery("from Municipio where id_estado=:estadoId");
		theQuery.setParameter("estadoId", id);

		List<Municipio> municipios = theQuery.getResultList();
		return municipios;
	}

}

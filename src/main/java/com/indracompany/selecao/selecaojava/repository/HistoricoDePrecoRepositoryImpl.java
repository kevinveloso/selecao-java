package com.indracompany.selecao.selecaojava.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.indracompany.selecao.selecaojava.entity.HistoricoDePreco;

@Repository
public class HistoricoDePrecoRepositoryImpl implements HistoricoDePrecoRepository {

	// dependencia entitymanager
	private EntityManager entityManager;
	
	// injecao no construtor
	@Autowired
	public HistoricoDePrecoRepositoryImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	public List<HistoricoDePreco> listAll() {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);

		// criar uma query de listagem
		Query<HistoricoDePreco> query = currentSession.createQuery("from HistoricoDePreco", HistoricoDePreco.class);

		// executar query e guardar o resultado
		List<HistoricoDePreco> results = query.getResultList();

		return results;
	}

	@Override
	public HistoricoDePreco getById(int id) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// selecionar historico do banco
		HistoricoDePreco historico = currentSession.get(HistoricoDePreco.class, id);
		
		return historico;
	}

	@Override
	public void addOrUpdate(HistoricoDePreco historico) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// limpando a sessao
		currentSession.flush();
		currentSession.clear();
		
		// adicionar ou atualizar historico
		currentSession.saveOrUpdate(historico);
	}

	@Override
	public void deleteById(int id) {

		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// apagar historico por id
		Query theQuery = currentSession.createQuery("delete from HistoricoDePreco where id=:historicoId");
		theQuery.setParameter("historicoId", id);
		
		theQuery.executeUpdate();
		
	}

	@Override
	public List<HistoricoDePreco> getAllByDate(Date data) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
				
		// pega historicos por data de coleta
		Query theQuery = currentSession.createQuery("from HistoricoDePreco where data_coleta=:dataColeta");
		theQuery.setParameter("dataColeta", data);

		List<HistoricoDePreco> historicos = theQuery.getResultList();
		return historicos;
	}

	@Override
	public List<HistoricoDePreco> getAllByInfoCodigo(int codigo) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);

		// pega historicos por data de coleta
		Query theQuery = currentSession.createQuery("from HistoricoDePreco where info_combustivel_codigo=:codigo");
		theQuery.setParameter("codigo", codigo);

		List<HistoricoDePreco> historicos = theQuery.getResultList();
		return historicos;
	}

}

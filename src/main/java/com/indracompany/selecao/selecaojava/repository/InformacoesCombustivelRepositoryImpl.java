package com.indracompany.selecao.selecaojava.repository;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.indracompany.selecao.selecaojava.entity.InformacoesCombustivel;

@Repository
public class InformacoesCombustivelRepositoryImpl implements InformacoesCombustivelRepository {

	// dependencia entitymanager
	private EntityManager entityManager;
	
	// injecao no construtor
	@Autowired
	public InformacoesCombustivelRepositoryImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	public List<InformacoesCombustivel> listAll() {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);

		// criar uma query de listagem
		Query<InformacoesCombustivel> query = currentSession.createQuery("from InformacoesCombustivel", InformacoesCombustivel.class);

		// executar query e guardar o resultado
		List<InformacoesCombustivel> results = query.getResultList();

		return results;
	}

	@Override
	public InformacoesCombustivel getByCode(int codigo) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// selecionar informacoes do combustivel do banco
		InformacoesCombustivel info = currentSession.get(InformacoesCombustivel.class, codigo);
		
		return info;
	}

	@Override
	public void addOrUpdate(InformacoesCombustivel info) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// limpando a sessao
		currentSession.flush();
		currentSession.clear();
		
		// adicionar ou atualizar informacoes do combustivel
		currentSession.saveOrUpdate(info);
	}

	@Override
	public void deleteByCode(int codigo) {

		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// apagar informacoes do combustivel por codigo
		Query theQuery = currentSession.createQuery("delete from InformacoesCombustivel where codigo=:codigo");
		theQuery.setParameter("codigo", codigo);
		
		theQuery.executeUpdate();

	}

	@Override
	public List<InformacoesCombustivel> getByAllByIdEmpresa(int id) {

		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// pega informacoes do combustivel por empresa
		Query theQuery = currentSession.createQuery("from InformacoesCombustivel where id_empresa=:id");
		theQuery.setParameter("id", id);

		List<InformacoesCombustivel> infos = theQuery.getResultList();
		return infos;
	}

	@Override
	public List<InformacoesCombustivel> getByAllByIdCombustivel(int id) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// pega informacoes do combustivel por combustivel
		Query theQuery = currentSession.createQuery("from InformacoesCombustivel where id_combustivel=:id");
		theQuery.setParameter("id", id);

		List<InformacoesCombustivel> infos = theQuery.getResultList();
		return infos;
	}

	@Override
	public List<InformacoesCombustivel> getByAllByIdBandeira(int id) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);

		// pega informacoes do combustivel por bandeira
		Query theQuery = currentSession.createQuery("from InformacoesCombustivel where id_bandeira=:id");
		theQuery.setParameter("id", id);

		List<InformacoesCombustivel> infos = theQuery.getResultList();
		return infos;
	}

}

package com.indracompany.selecao.selecaojava.repository;

import java.util.Date;
import java.util.List;

import com.indracompany.selecao.selecaojava.entity.HistoricoDePreco;

public interface HistoricoDePrecoRepository {
	
	public List<HistoricoDePreco> listAll();
	
	public HistoricoDePreco getById(int id);
	
	public void addOrUpdate(HistoricoDePreco historico);
	
	public void deleteById(int id);
	
	public List<HistoricoDePreco> getAllByDate(Date data);
	
	public List<HistoricoDePreco> getAllByInfoCodigo(int codigo);

}

package com.indracompany.selecao.selecaojava.repository;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.indracompany.selecao.selecaojava.entity.User;

@Repository
public class UserRepositoryImpl implements UserRepository {

	// dependencia entitymanager
	private EntityManager entityManager;
	
	// injecao no construtor
	@Autowired
	public UserRepositoryImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	public List<User> listAll() {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// criar uma query de listagem
		Query<User> query = currentSession.createQuery("from User", User.class);
		
		// executar query e guardar o resultado
		List<User> results = query.getResultList();
		
		return results;
	}

	@Override
	public User getById(int id) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// selecionar usuario do banco
		User user = currentSession.get(User.class, id);
		
		return user;
	}

	@Override
	public void addOrUpdate(User user) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// limpando a sessao
		currentSession.flush();
		currentSession.clear();
		
		// adicionar ou atualizar o usuario
		currentSession.saveOrUpdate(user);
	}

	@Override
	public void deleteById(int id) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// apagar usuario por id
		Query theQuery = currentSession.createQuery("delete from User where id=:userId");
		theQuery.setParameter("userId", id);
		
		theQuery.executeUpdate();
	}

}

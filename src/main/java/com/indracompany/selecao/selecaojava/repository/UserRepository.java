package com.indracompany.selecao.selecaojava.repository;

import java.util.List;

import com.indracompany.selecao.selecaojava.entity.User;

public interface UserRepository {

	public List<User> listAll();
	
	public User getById(int id);
	
	public void addOrUpdate(User user);
	
	public void deleteById(int id);
	
}

package com.indracompany.selecao.selecaojava.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.indracompany.selecao.selecaojava.entity.Estado;

@Repository
public class EstadoRepositoryImpl implements EstadoRepository {

	// dependencia entitymanager
	private EntityManager entityManager;
	
	// injecao no construtor
	@Autowired
	public EstadoRepositoryImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	public List<Estado> listAll() {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);

		// criar uma query de listagem
		Query<Estado> query = currentSession.createQuery("from Estado", Estado.class);

		// executar query e guardar o resultado
		List<Estado> results = query.getResultList();

		return results;
	}

	@Override
	public Estado getById(int id) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// selecionar estado do banco
		Estado estado = currentSession.get(Estado.class, id);
		
		return estado;
	}

	@Override
	public void addOrUpdate(Estado estado) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// limpando a sessao
		currentSession.flush();
		currentSession.clear();
		
		// adicionar ou atualizar estado
		currentSession.saveOrUpdate(estado);
	}

	@Override
	public void deleteById(int id) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// apagar estado por id
		Query theQuery = currentSession.createQuery("delete from Estado where id=:estadoId");
		theQuery.setParameter("estadoId", id);
		
		theQuery.executeUpdate();
	}

	@Override
	public Estado getByInitials(String sigla) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// pega estado por sigla
		Query theQuery = currentSession.createQuery("from Estado where lower(sigla)=:sigla");
		theQuery.setParameter("sigla", sigla.toLowerCase());
		
		try {
			Estado estado = (Estado) theQuery.getSingleResult();
			return estado;
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public List<Estado> getAllByRegiaoId(int id) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// pega estados por regiao
		Query theQuery = currentSession.createQuery("from Estado where id_regiao=:regiaoId");
		theQuery.setParameter("regiaoId", id);

		List<Estado> estados = theQuery.getResultList();
		return estados;
	}

}

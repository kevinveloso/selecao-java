package com.indracompany.selecao.selecaojava.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.indracompany.selecao.selecaojava.entity.Regiao;

@Repository
public class RegiaoRepositoryImpl implements RegiaoRepository {

	// dependencia entitymanager
	private EntityManager entityManager;
	
	// injecao no construtor
	@Autowired
	public RegiaoRepositoryImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	public List<Regiao> listAll() {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);

		// criar uma query de listagem
		Query<Regiao> query = currentSession.createQuery("from Regiao", Regiao.class);

		// executar query e guardar o resultado
		List<Regiao> results = query.getResultList();

		return results;
	}

	@Override
	public Regiao getById(int id) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// selecionar regiao do banco
		Regiao regiao = currentSession.get(Regiao.class, id);
		
		return regiao;
	}

	@Override
	public void addOrUpdate(Regiao regiao) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// limpando a sessao
		currentSession.flush();
		currentSession.clear();
		
		// adicionar ou atualizar regiao
		currentSession.saveOrUpdate(regiao);
	}

	@Override
	public void deleteById(int id) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// apagar regiao por id
		Query theQuery = currentSession.createQuery("delete from Regiao where id=:regiaoId");
		theQuery.setParameter("regiaoId", id);
		
		theQuery.executeUpdate();
	}

	@Override
	public Regiao getByInitials(String sigla) {
		// pegar a sessao do hibernate
		Session currentSession = entityManager.unwrap(Session.class);
		
		// pega regiao por sigla
		Query theQuery = currentSession.createQuery("from Regiao where lower(sigla)=:sigla");
		theQuery.setParameter("sigla", sigla.toLowerCase());
		
		try {
			Regiao regiao = (Regiao) theQuery.getSingleResult();
			return regiao;
		} catch (NoResultException e) {
			return null;
		}
		
	}

}

package com.indracompany.selecao.selecaojava.repository;

import java.util.List;

import com.indracompany.selecao.selecaojava.entity.Combustivel;

public interface CombustivelRepository {
	
	public List<Combustivel> listAll();

	public Combustivel getById(int id);

	public void addOrUpdate(Combustivel combustivel);

	public void deleteById(int id);

	public Combustivel getByName(String nome);

}

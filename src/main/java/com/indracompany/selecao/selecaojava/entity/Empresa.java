package com.indracompany.selecao.selecaojava.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="empresa")
public class Empresa {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="nome", unique = true)
	private String nome;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_municipio")
    @JsonBackReference
	private Municipio municipio;
	
	@OneToMany(mappedBy="empresa",cascade=CascadeType.ALL)
	@JsonManagedReference
	private List<InformacoesCombustivel> infoCombustiveis;

	public Empresa() {
	}

	public Empresa(String nome, Municipio municipio) {
		this.nome = nome;
		this.municipio = municipio;
		this.infoCombustiveis = new ArrayList<InformacoesCombustivel>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public List<InformacoesCombustivel> getInfoCombustiveis() {
		return infoCombustiveis;
	}

	public void setInfoCombustiveis(List<InformacoesCombustivel> infoCombustiveis) {
		this.infoCombustiveis = infoCombustiveis;
	}

	@Override
	public String toString() {
		return "Empresa [id=" + id + ", nome=" + nome + ", municipio=" + municipio + ", infoCombustiveis="
				+ infoCombustiveis + "]";
	}
	
}

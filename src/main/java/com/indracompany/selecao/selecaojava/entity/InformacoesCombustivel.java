package com.indracompany.selecao.selecaojava.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="info_combustivel")
public class InformacoesCombustivel {
	
	@Id
	@Column(name="codigo")
	private int codigo;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_empresa")
    @JsonBackReference
	private Empresa empresa;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_combustivel")
    @JsonBackReference
	private Combustivel combustivel;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_bandeira")
    @JsonBackReference
	private Bandeira bandeira;
	
	@OneToMany(mappedBy="infoCombustivel",cascade=CascadeType.ALL)
	@JsonManagedReference
	private List<HistoricoDePreco> historicos;

	public InformacoesCombustivel() {
	}

	public InformacoesCombustivel(int codigo, Empresa empresa, Combustivel combustivel, Bandeira bandeira) {
		this.codigo = codigo;
		this.empresa = empresa;
		this.combustivel = combustivel;
		this.bandeira = bandeira;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Combustivel getCombustivel() {
		return combustivel;
	}

	public void setCombustivel(Combustivel combustivel) {
		this.combustivel = combustivel;
	}

	public Bandeira getBandeira() {
		return bandeira;
	}

	public void setBandeira(Bandeira bandeira) {
		this.bandeira = bandeira;
	}
	
	public List<HistoricoDePreco> getHistorico() {
		return historicos;
	}

	public void setHistorico(List<HistoricoDePreco> historicos) {
		this.historicos = historicos;
	}

	@Override
	public String toString() {
		return "InformacoesCombustivel [codigo=" + codigo + ", empresa=" + empresa + ", combustivel=" + combustivel
				+ ", bandeira=" + bandeira + ", historicos=" + historicos + "]";
	}

}

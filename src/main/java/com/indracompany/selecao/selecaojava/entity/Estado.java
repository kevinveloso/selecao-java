package com.indracompany.selecao.selecaojava.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="estado")
public class Estado {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="sigla",unique = true)
	private String sigla;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_regiao")
	@JsonBackReference
	private Regiao regiao;
	
	@OneToMany(mappedBy="estado",cascade=CascadeType.ALL)
	@JsonManagedReference
	private List<Municipio> municipios;

	public Estado() {
	}

	public Estado(String sigla, Regiao regiao) {
		this.sigla = sigla;
		this.regiao = regiao;
		this.municipios =  new ArrayList<Municipio>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public Regiao getRegiao() {
		return regiao;
	}

	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}

	public List<Municipio> getMunicipios() {
		return municipios;
	}

	public void setMunicipios(List<Municipio> municipios) {
		this.municipios = municipios;
	}

	@Override
	public String toString() {
		return "Estado [id=" + id + ", sigla=" + sigla + ", regiao=" + regiao + ", municipios="
				+ municipios + "]";
	}
	
}

package com.indracompany.selecao.selecaojava.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="municipio", uniqueConstraints = {@UniqueConstraint(columnNames = {"nome", "id_estado"})})
public class Municipio {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="nome")
	private String nome;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_estado")
    @JsonBackReference
	private Estado estado;
	
	@OneToMany(mappedBy="municipio",cascade=CascadeType.ALL)
	@JsonManagedReference
	private List<Empresa> empresas;

	public Municipio() {
	}

	public Municipio(String nome, Estado estado) {
		this.nome = nome;
		this.estado = estado;
		this.empresas = new ArrayList<Empresa>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public List<Empresa> getEmpresas() {
		return empresas;
	}

	public void setEmpresas(List<Empresa> empresas) {
		this.empresas = empresas;
	}

	@Override
	public String toString() {
		return "Municipio [id=" + id + ", nome=" + nome + ", estado=" + estado + ", empresas=" + empresas + "]";
	}

}

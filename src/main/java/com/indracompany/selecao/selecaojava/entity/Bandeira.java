package com.indracompany.selecao.selecaojava.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="bandeira")
public class Bandeira {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="nome", unique=true)
	private String nome;

	@OneToMany(mappedBy="bandeira",cascade=CascadeType.ALL)
	@JsonManagedReference
	private List<InformacoesCombustivel> infoCombustiveis;
	
	public Bandeira() {
	}

	public Bandeira(String nome) {
		this.nome = nome;
		this.infoCombustiveis = new ArrayList<InformacoesCombustivel>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<InformacoesCombustivel> getInfoCombustiveis() {
		return infoCombustiveis;
	}

	public void setInfoCombustiveis(List<InformacoesCombustivel> infoCombustiveis) {
		this.infoCombustiveis = infoCombustiveis;
	}

	@Override
	public String toString() {
		return "Bandeira [id=" + id + ", nome=" + nome + ", infoCombustiveis=" + infoCombustiveis + "]";
	}

}

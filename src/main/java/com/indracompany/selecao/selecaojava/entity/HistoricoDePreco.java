package com.indracompany.selecao.selecaojava.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="historico_de_precos")
public class HistoricoDePreco {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "info_combustivel_codigo")
	@JsonBackReference
	private InformacoesCombustivel infoCombustivel;
	
	@Column(name="data_coleta")
	@Temporal(TemporalType.DATE)
	private Date dataColeta;
	
	@Column(name="valor_compra")
	private Float valorCompra;
	
	@Column(name="valor_venda")
	private Float valorVenda;
	
	public HistoricoDePreco() {
	}

	public HistoricoDePreco(InformacoesCombustivel infoCombustivel, Date dataColeta, Float valorCompra,
			Float valorVenda) {
		this.infoCombustivel = infoCombustivel;
		this.dataColeta = dataColeta;
		this.valorCompra = valorCompra;
		this.valorVenda = valorVenda;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public InformacoesCombustivel getInfoCombustivel() {
		return infoCombustivel;
	}

	public void setInfoCombustivel(InformacoesCombustivel infoCombustivel) {
		this.infoCombustivel = infoCombustivel;
	}

	public Date getDataColeta() {
		return dataColeta;
	}

	public void setDataColeta(Date dataColeta) {
		this.dataColeta = dataColeta;
	}

	public Float getValorCompra() {
		return valorCompra;
	}

	public void setValorCompra(Float valorCompra) {
		this.valorCompra = valorCompra;
	}

	public Float getValorVenda() {
		return valorVenda;
	}

	public void setValorVenda(Float valorVenda) {
		this.valorVenda = valorVenda;
	}

	@Override
	public String toString() {
		return "HistoricoDePrecos [id=" + id + ", infoCombustivel=" + infoCombustivel + ", dataColeta=" + dataColeta
				+ ", valorCompra=" + valorCompra + ", valorVenda=" + valorVenda + "]";
	}
	
}

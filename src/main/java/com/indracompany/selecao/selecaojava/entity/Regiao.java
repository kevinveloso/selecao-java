package com.indracompany.selecao.selecaojava.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="regiao")
public class Regiao {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="sigla", unique = true)
	private String sigla;
	
	@OneToMany(mappedBy="regiao",cascade=CascadeType.ALL)
	@JsonManagedReference
	private List<Estado> estados;

	public Regiao() {
	}

	public Regiao(String sigla) {
		this.sigla = sigla;
		this.estados = new ArrayList<Estado>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public List<Estado> getEstados() {
		return estados;
	}

	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}

	@Override
	public String toString() {
		return "Regiao [id=" + id + ", sigla=" + sigla + ", estados=" + estados + "]";
	}

}

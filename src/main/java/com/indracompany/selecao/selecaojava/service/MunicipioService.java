package com.indracompany.selecao.selecaojava.service;

import java.util.List;

import com.indracompany.selecao.selecaojava.entity.Municipio;

public interface MunicipioService {
	
	public List<Municipio> listAll();
	
	public Municipio getById(int id);
	
	public void addOrUpdate(Municipio municipio);
	
	public void deleteById(int id);

	public Municipio getByName(String nome);
	
	public Municipio getByNameAndEstadoId(String nome, int estadoId);
	
	public List<Municipio> getAllByEstadoId(int id);
	
}

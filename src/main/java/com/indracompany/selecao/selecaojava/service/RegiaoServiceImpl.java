package com.indracompany.selecao.selecaojava.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indracompany.selecao.selecaojava.entity.Regiao;
import com.indracompany.selecao.selecaojava.repository.RegiaoRepository;

@Service
public class RegiaoServiceImpl implements RegiaoService {

	private RegiaoRepository regiaoRepository;

	@Autowired
	public RegiaoServiceImpl(RegiaoRepository regiaoRepository) {
		this.regiaoRepository = regiaoRepository;
	}
	
	@Override
	@Transactional
	public List<Regiao> listAll() {
		return regiaoRepository.listAll();
	}

	@Override
	@Transactional
	public Regiao getById(int id) {
		return regiaoRepository.getById(id);
	}

	@Override
	@Transactional
	public void addOrUpdate(Regiao regiao) {
		regiaoRepository.addOrUpdate(regiao);
	}

	@Override
	@Transactional
	public void deleteById(int id) {
		regiaoRepository.deleteById(id);
	}

	@Override
	@Transactional
	public Regiao getByInitials(String sigla) {
		return regiaoRepository.getByInitials(sigla);
	}

}

package com.indracompany.selecao.selecaojava.service;

import java.util.List;

import com.indracompany.selecao.selecaojava.entity.InformacoesCombustivel;

public interface InformacoesCombustivelService {
	
	public List<InformacoesCombustivel> listAll();
	
	public InformacoesCombustivel getByCode(int codigo);
	
	public void addOrUpdate(InformacoesCombustivel info);
	
	public void deleteByCode(int codigo);
	
	public List<InformacoesCombustivel> getByAllByIdEmpresa(int id);
	
	public List<InformacoesCombustivel> getByAllByIdCombustivel(int id);
	
	public List<InformacoesCombustivel> getByAllByIdBandeira(int id);
	
}

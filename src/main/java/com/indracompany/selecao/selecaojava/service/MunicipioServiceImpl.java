package com.indracompany.selecao.selecaojava.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indracompany.selecao.selecaojava.entity.Municipio;
import com.indracompany.selecao.selecaojava.repository.MunicipioRepository;

@Service
public class MunicipioServiceImpl implements MunicipioService {

	private MunicipioRepository municipioRepository;

	@Autowired
	public MunicipioServiceImpl(MunicipioRepository municipioRepository) {
		this.municipioRepository = municipioRepository;
	}
	
	@Override
	@Transactional
	public List<Municipio> listAll() {
		return municipioRepository.listAll();
	}

	@Override
	@Transactional
	public Municipio getById(int id) {
		return municipioRepository.getById(id);
	}

	@Override
	@Transactional
	public void addOrUpdate(Municipio municipio) {
		municipioRepository.addOrUpdate(municipio);
	}

	@Override
	@Transactional
	public void deleteById(int id) {
		municipioRepository.deleteById(id);
	}

	@Override
	@Transactional
	public Municipio getByName(String nome) {
		return municipioRepository.getByName(nome);
	}
	
	@Override
	@Transactional
	public Municipio getByNameAndEstadoId(String nome, int estadoId) {
		return municipioRepository.getByNameAndEstadoId(nome, estadoId);
	}

	@Override
	@Transactional
	public List<Municipio> getAllByEstadoId(int id) {
		return municipioRepository.getAllByEstadoId(id);
	}

}

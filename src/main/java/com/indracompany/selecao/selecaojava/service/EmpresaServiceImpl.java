package com.indracompany.selecao.selecaojava.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indracompany.selecao.selecaojava.entity.Empresa;
import com.indracompany.selecao.selecaojava.repository.EmpresaRepository;

@Service
public class EmpresaServiceImpl implements EmpresaService {
	
	private EmpresaRepository empresaRepository;

	@Autowired
	public EmpresaServiceImpl(EmpresaRepository empresaRepository) {
		this.empresaRepository = empresaRepository;
	}
	
	@Override
	@Transactional
	public List<Empresa> listAll() {
		return empresaRepository.listAll();
	}

	@Override
	@Transactional
	public Empresa getById(int id) {
		return empresaRepository.getById(id);
	}

	@Override
	@Transactional
	public void addOrUpdate(Empresa empresa) {
		empresaRepository.addOrUpdate(empresa);
	}

	@Override
	@Transactional
	public void deleteById(int id) {
		empresaRepository.deleteById(id);
	}

	@Override
	@Transactional
	public Empresa getByName(String nome) {
		return empresaRepository.getByName(nome);
	}

	@Override
	@Transactional
	public List<Empresa> getAllByMunicipioId(int id) {
		return empresaRepository.getAllByMunicipioId(id);
	}

}

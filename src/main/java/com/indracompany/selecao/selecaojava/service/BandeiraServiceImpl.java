package com.indracompany.selecao.selecaojava.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indracompany.selecao.selecaojava.entity.Bandeira;
import com.indracompany.selecao.selecaojava.repository.BandeiraRepository;

@Service
public class BandeiraServiceImpl implements BandeiraService {
	
	private BandeiraRepository bandeiraRepository;

	@Autowired
	public BandeiraServiceImpl(BandeiraRepository bandeiraRepository) {
		this.bandeiraRepository = bandeiraRepository;
	}
	
	@Override
	@Transactional
	public List<Bandeira> listAll() {
		return bandeiraRepository.listAll();
	}

	@Override
	@Transactional
	public Bandeira getById(int id) {
		return bandeiraRepository.getById(id);
	}

	@Override
	@Transactional
	public void addOrUpdate(Bandeira bandeira) {
		bandeiraRepository.addOrUpdate(bandeira);
	}

	@Override
	@Transactional
	public void deleteById(int id) {
		bandeiraRepository.deleteById(id);
	}

	@Override
	@Transactional
	public Bandeira getByName(String nome) {
		return bandeiraRepository.getByName(nome);
	}

}

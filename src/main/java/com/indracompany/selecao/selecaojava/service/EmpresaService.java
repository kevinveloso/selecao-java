package com.indracompany.selecao.selecaojava.service;

import java.util.List;

import com.indracompany.selecao.selecaojava.entity.Empresa;

public interface EmpresaService {
	
	public List<Empresa> listAll();
	
	public Empresa getById(int id);
	
	public void addOrUpdate(Empresa empresa);
	
	public void deleteById(int id);
	
	public Empresa getByName(String nome);

	public List<Empresa> getAllByMunicipioId(int id);
	
}

package com.indracompany.selecao.selecaojava.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indracompany.selecao.selecaojava.entity.InformacoesCombustivel;
import com.indracompany.selecao.selecaojava.repository.InformacoesCombustivelRepository;

@Service
public class InformacoesCombustivelServiceImpl implements InformacoesCombustivelService {

	private InformacoesCombustivelRepository informacoesCombustivelRepository;

	@Autowired
	public InformacoesCombustivelServiceImpl(InformacoesCombustivelRepository informacoesCombustivelRepository) {
		this.informacoesCombustivelRepository = informacoesCombustivelRepository;
	}
	
	@Override
	@Transactional
	public List<InformacoesCombustivel> listAll() {
		return informacoesCombustivelRepository.listAll();
	}

	@Override
	@Transactional
	public InformacoesCombustivel getByCode(int codigo) {
		return informacoesCombustivelRepository.getByCode(codigo);
	}

	@Override
	@Transactional
	public void addOrUpdate(InformacoesCombustivel info) {
		informacoesCombustivelRepository.addOrUpdate(info);
	}

	@Override
	@Transactional
	public void deleteByCode(int codigo) {
		informacoesCombustivelRepository.deleteByCode(codigo);
	}

	@Override
	@Transactional
	public List<InformacoesCombustivel> getByAllByIdEmpresa(int id) {
		return informacoesCombustivelRepository.getByAllByIdEmpresa(id);
	}

	@Override
	@Transactional
	public List<InformacoesCombustivel> getByAllByIdCombustivel(int id) {
		return informacoesCombustivelRepository.getByAllByIdCombustivel(id);
	}

	@Override
	@Transactional
	public List<InformacoesCombustivel> getByAllByIdBandeira(int id) {
		return informacoesCombustivelRepository.getByAllByIdBandeira(id);
	}

}

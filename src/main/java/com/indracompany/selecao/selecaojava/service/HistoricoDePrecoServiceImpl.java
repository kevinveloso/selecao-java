package com.indracompany.selecao.selecaojava.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indracompany.selecao.selecaojava.entity.HistoricoDePreco;
import com.indracompany.selecao.selecaojava.repository.HistoricoDePrecoRepository;

@Service
public class HistoricoDePrecoServiceImpl implements HistoricoDePrecoService {

	private HistoricoDePrecoRepository historicoDePrecoRepository;

	@Autowired
	public HistoricoDePrecoServiceImpl(HistoricoDePrecoRepository historicoDePrecoRepository) {
		this.historicoDePrecoRepository = historicoDePrecoRepository;
	}
	
	@Override
	@Transactional
	public List<HistoricoDePreco> listAll() {
		return historicoDePrecoRepository.listAll();
	}

	@Override
	@Transactional
	public HistoricoDePreco getById(int id) {
		return historicoDePrecoRepository.getById(id);
	}

	@Override
	@Transactional
	public void addOrUpdate(HistoricoDePreco historico) {
		historicoDePrecoRepository.addOrUpdate(historico);
	}

	@Override
	@Transactional
	public void deleteById(int id) {
		historicoDePrecoRepository.deleteById(id);
	}

	@Override
	@Transactional
	public List<HistoricoDePreco> getAllByDate(Date data) {
		return historicoDePrecoRepository.getAllByDate(data);
	}

	@Override
	@Transactional
	public List<HistoricoDePreco> getAllByInfoCodigo(int codigo) {
		return historicoDePrecoRepository.getAllByInfoCodigo(codigo);
	}

}

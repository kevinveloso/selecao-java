package com.indracompany.selecao.selecaojava.service;

import java.util.List;

import com.indracompany.selecao.selecaojava.entity.Estado;

public interface EstadoService {

	public List<Estado> listAll();
	
	public Estado getById(int id);
	
	public void addOrUpdate(Estado estado);
	
	public void deleteById(int id);
	
	public Estado getByInitials(String sigla);
	
	public List<Estado> getAllByRegiaoId(int id);
	
}

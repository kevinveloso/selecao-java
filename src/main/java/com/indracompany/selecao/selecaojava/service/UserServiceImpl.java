package com.indracompany.selecao.selecaojava.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indracompany.selecao.selecaojava.entity.User;
import com.indracompany.selecao.selecaojava.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	private UserRepository userRepository;
	
	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	@Transactional
	public List<User> listAll() {
		return userRepository.listAll();
	}

	@Override
	@Transactional
	public User getById(int id) {
		return userRepository.getById(id);
	}

	@Override
	@Transactional
	public void addOrUpdate(User user) {
		this.userRepository.addOrUpdate(user);
	}

	@Override
	@Transactional
	public void deleteById(int id) {
		this.userRepository.deleteById(id);
	}

}

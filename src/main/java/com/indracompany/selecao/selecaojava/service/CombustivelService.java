package com.indracompany.selecao.selecaojava.service;

import java.util.List;

import com.indracompany.selecao.selecaojava.entity.Combustivel;

public interface CombustivelService {
	
	public List<Combustivel> listAll();

	public Combustivel getById(int id);

	public void addOrUpdate(Combustivel combustivel);

	public void deleteById(int id);

	public Combustivel getByName(String nome);
}

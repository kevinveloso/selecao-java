package com.indracompany.selecao.selecaojava.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indracompany.selecao.selecaojava.entity.Combustivel;
import com.indracompany.selecao.selecaojava.repository.CombustivelRepository;

@Service
public class CombustivelServiceImpl implements CombustivelService {
	
	private CombustivelRepository combustivelRepository;

	@Autowired
	public CombustivelServiceImpl(CombustivelRepository combustivelRepository) {
		this.combustivelRepository = combustivelRepository;
	}
	
	@Override
	@Transactional
	public List<Combustivel> listAll() {
		return combustivelRepository.listAll();
	}

	@Override
	@Transactional
	public Combustivel getById(int id) {
		return combustivelRepository.getById(id);
	}

	@Override
	@Transactional
	public void addOrUpdate(Combustivel combustivel) {
		combustivelRepository.addOrUpdate(combustivel);
	}

	@Override
	@Transactional
	public void deleteById(int id) {
		combustivelRepository.deleteById(id);
	}

	@Override
	@Transactional
	public Combustivel getByName(String nome) {
		return combustivelRepository.getByName(nome);
	}

}

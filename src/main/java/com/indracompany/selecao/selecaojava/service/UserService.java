package com.indracompany.selecao.selecaojava.service;

import java.util.List;

import com.indracompany.selecao.selecaojava.entity.User;

public interface UserService {
	
	public List<User> listAll();
	
	public User getById(int id);
	
	public void addOrUpdate(User user);
	
	public void deleteById(int id);
}

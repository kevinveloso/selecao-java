package com.indracompany.selecao.selecaojava.service;

import java.util.List;

import com.indracompany.selecao.selecaojava.entity.Bandeira;

public interface BandeiraService {
	
	public List<Bandeira> listAll();
	
	public Bandeira getById(int id);
	
	public void addOrUpdate(Bandeira bandeira);
	
	public void deleteById(int id);
	
	public Bandeira getByName(String nome);
	
}

package com.indracompany.selecao.selecaojava.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indracompany.selecao.selecaojava.entity.Estado;
import com.indracompany.selecao.selecaojava.repository.EstadoRepository;

@Service
public class EstadoServiceImpl implements EstadoService {

	private EstadoRepository estadoRepository;

	@Autowired
	public EstadoServiceImpl(EstadoRepository estadoRepository) {
		this.estadoRepository = estadoRepository;
	}
	
	@Override
	@Transactional
	public List<Estado> listAll() {
		return estadoRepository.listAll();
	}

	@Override
	@Transactional
	public Estado getById(int id) {
		return estadoRepository.getById(id);
	}

	@Override
	@Transactional
	public void addOrUpdate(Estado estado) {
		estadoRepository.addOrUpdate(estado);
	}

	@Override
	@Transactional
	public void deleteById(int id) {
		estadoRepository.deleteById(id);
	}

	@Override
	@Transactional
	public Estado getByInitials(String sigla) {
		return estadoRepository.getByInitials(sigla);
	}

	@Override
	@Transactional
	public List<Estado> getAllByRegiaoId(int id) {
		return estadoRepository.getAllByRegiaoId(id);
	}

}

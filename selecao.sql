
DROP TABLE IF EXISTS usuario;

CREATE TABLE usuario (
  id int(11) NOT NULL AUTO_INCREMENT,
  login varchar(45) DEFAULT NULL,
  nome varchar(45) DEFAULT NULL,
  senha varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8MB4;

INSERT INTO usuario VALUES 
	(1,'usuario_um','Nome Um','abcdef'),
	(2,'usuario_dois','Nome Dois','123456'),
	(3,'usuario_tres','Nome Tres','admin'),
	(4,'usuario_quatro','Nome Quatro','password'),
	(5,'usuario_cinco','Nome Cinco','senha');

DROP TABLE IF EXISTS bandeira;

CREATE TABLE bandeira (
  id int(11) NOT NULL AUTO_INCREMENT,
  nome varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8MB4;

DROP TABLE IF EXISTS localizacao;

CREATE TABLE localizacao (
  id int(11) NOT NULL AUTO_INCREMENT,
  regiao varchar(45) DEFAULT NULL,
  uf varchar(45) DEFAULT NULL,
  municipio varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8MB4;

DROP TABLE IF EXISTS empresa;

CREATE TABLE empresa (
  id int(11) NOT NULL AUTO_INCREMENT,
  id_localizacao int(11) DEFAULT NULL,
  nome varchar(255) DEFAULT NULL,
  id_bandeira int(11) DEFAULT NULL,
  FOREIGN KEY (id_localizacao) REFERENCES localizacao(id),
  FOREIGN KEY (id_bandeira) REFERENCES bandeira(id),
  PRIMARY KEY (id)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8MB4;

DROP TABLE IF EXISTS produto;

CREATE TABLE produto (
  id int(11) NOT NULL AUTO_INCREMENT,
  nome varchar(255) DEFAULT NULL,
  unidade_medida varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8MB4;

DROP TABLE IF EXISTS historico_preco;

CREATE TABLE historico_preco (
  id int(11) NOT NULL AUTO_INCREMENT,
  id_empresa int(11) DEFAULT NULL,
  codigo varchar(45) DEFAULT NULL,
  id_produto int(11) DEFAULT NULL,
  data varchar(45) DEFAULT NULL,
  valor_compra decimal(10,6) DEFAULT NULL,
  valor_venda decimal(10,6) DEFAULT NULL,
  FOREIGN KEY (id_empresa) REFERENCES empresa(id),
  FOREIGN KEY (id_produto) REFERENCES produto(id),
  PRIMARY KEY (id)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8MB4;